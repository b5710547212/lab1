package oop.lab1;

//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Student other) {
		return this.id == other.id;
	}
}
